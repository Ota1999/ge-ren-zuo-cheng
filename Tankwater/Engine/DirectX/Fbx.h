#pragma once
#include <d3dx9.h>
#include <fbxsdk.h>
#include <vector>
#include <map>

#pragma comment(lib,"libfbxsdk-mt.lib")

class FbxParts;

//レイキャスト用構造体
struct RayCastData
{
	D3DXVECTOR3 start;	//レイ発射位置
	D3DXVECTOR3 dir;	//レイの向きベクトル
	float       dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか
	D3DXVECTOR3 normal;	//法線

	RayCastData() { dist = 99999.0f; }
};


//ひとつのFBXファイルを扱うクラス
class Fbx 
{
	friend class FbxParts;

	LPD3DXEFFECT	pEffect_;

	//ロードに必要なやつら
	FbxManager*  _pManager;
	FbxImporter* _pImporter;
	FbxScene*    _pScene;

	// アニメーションのフレームレート
	FbxTime::EMode	_frameRate;

	//アニメーション速度
	float			_animSpeed;

	//アニメーションの最初と最後のフレーム
	int _startFrame, _endFrame;

	//パーツ（複数あるかも）
	std::vector<FbxParts*>	parts;

	//テクスチャ（複数あるかも）
	std::map<std::string, LPDIRECT3DTEXTURE9> _pTexture;




	//ノードの中身を調べる
	//引数：pNode		調べるノード
	//引数：pPartsList	パーツのリスト
	void CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList);

	//テクスチャのロード
	//引数：fileName	ファイル名
	//戻値：テクスチャのアドレス
	LPDIRECT3DTEXTURE9 LoadTexture(std::string fileName);



public:
	//コンストラクタ
	Fbx(LPD3DXEFFECT pEffect);

	//デストラクタ
	~Fbx();

	//モデルをロードする
	//引数：fileName	ファイル名
	//戻値：成功／失敗
	HRESULT Load(std::string fileName);

	//描画
	//引数：matrix	ワールド行列
	//引数：frame	アニメーションの現在のフレーム
	void Draw(D3DXMATRIX matrix, int frame);

	//アニメーションの最初と最後のフレームを指定
	//引数：startFrame	最初のフレーム
	//引数：endFrame	最後のフレーム
	//引数：speed		アニメーション速度
	void SetAnimFrame(int startFrame, int endFrame, float speed);

	//任意のボーンの位置を取得
	//引数：boneName	取得したいボーンの位置
	//戻値：ボーンの位置
	D3DXVECTOR3 GetBonePosition(std::string boneName);

	//レイキャスト（レイを飛ばして当たり判定）
	//引数：data	必要なものをまとめたデータ
	void RayCast(RayCastData *data);
};
