#include "BoxCollider.h"
#include "SphereCollider.h"
#include "gameObject.h"
#include "../DirectX/Direct3D.h"

//コンストラクタ
Collider::Collider():
	_pGameObject(nullptr),_pMesh(nullptr)
{
}

//デストラクタ
Collider::~Collider()
{
	if (_pMesh != nullptr)
	{
		_pMesh->Release();
	}
}

//箱型同士の衝突判定
//引数：boxA	１つ目の箱型判定
//引数：boxB	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB)
{
	D3DXVECTOR3 boxPosA = boxA->_pGameObject->GetPosition() + boxA->_center;
	D3DXVECTOR3 boxPosB = boxB->_pGameObject->GetPosition() + boxB->_center;

	if ((boxPosA.x + boxA->_size.x) > boxPosB.x &&
		boxPosA.x < (boxB->_center.x + boxPosB.x) &&
		(boxPosA.y + boxA->_size.y) > boxPosB.y &&
		boxPosA.y < (boxB->_center.y + boxPosB.y) &&
		(boxPosA.z + boxA->_size.z) > boxPosB.z &&
		boxPosA.z < (boxB->_center.z + boxPosB.z))
	{
		return true;
	}
	return false;
}

//箱型と球体の衝突判定
//引数：box	箱型判定
//引数：sphere	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere)
{
	D3DXVECTOR3 circlePos = sphere->_pGameObject->GetPosition() + sphere->_center;
	D3DXVECTOR3 boxPos = box->_pGameObject->GetPosition() + box->_center;


	if (circlePos.x > boxPos.x - box->_size.x - sphere->_radius &&
		circlePos.x < boxPos.x + box->_size.x + sphere->_radius &&
		circlePos.y > boxPos.y - box->_size.y - sphere->_radius &&
		circlePos.y < boxPos.y + box->_size.y + sphere->_radius &&
		circlePos.z > boxPos.z - box->_size.z - sphere->_radius &&
		circlePos.z < boxPos.z + box->_size.z + sphere->_radius )
	{
		return true;
	}

	return false;
}

//球体同士の衝突判定
//引数：circleA	１つ目の球体判定
//引数：circleB	２つ目の球体判定
//戻値：接触していればtrue
bool Collider::IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB)
{
	D3DXVECTOR3 v = (circleA->_center + circleA->_pGameObject->GetPosition()) 
		- (circleB->_center + circleB->_pGameObject->GetPosition());

	if (D3DXVec3Length(&v) <= (circleA->_radius + circleB->_radius))
	{
		return true;
	}

	return false;
}

//テスト表示用の枠を描画
//引数：position	位置
void Collider::Draw(D3DXVECTOR3 position)
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, position.x + _center.x, position.y + _center.y, position.z + _center.z);
	Direct3D::pDevice_->SetTransform(D3DTS_WORLD, &mat);
	_pMesh->DrawSubset(0);
}
