#pragma once

#include "Engine/global.h"


class Block;

class Boost;

class Item_count;

class Tank;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//ポインタ
	Block* pBlock;
	Boost* pBoost;
	Item_count* pItem_Count;
	Tank* pTank;

	const int ITEM_NUM;	//アイテムの数


public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ブロックを下げる判定
	Block* GetBlock()
	{
		return pBlock;
	}

	Boost* GetBoost()
	{
		return pBoost;
	}

	Item_count* GetItem_Count()
	{
		return pItem_Count;
	}

	Tank* GetTank()
	{
		return pTank;
	}

};