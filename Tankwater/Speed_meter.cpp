#include "Speed_meter.h"
#include "Speed_meter.h"
#include "Engine/DirectX/Text.h"
#include "Engine/ResouceManager/Image.h"
//コンストラクタ
Speed_meter::Speed_meter(IGameObject * parent)
	:IGameObject(parent, "Speed_meter")
	, _hPict(-1), _hPict2(-1), _hPict3(-1),Boost(1)
{
}

//デストラクタ
Speed_meter::~Speed_meter()
{

}

//初期化
void Speed_meter::Initialize()
{

	//////////////////////////////////////////////////
	//画像表示関連
	_position = D3DXVECTOR3(30, 30, 0);

	//本体ゲージ画像データのロード
	_hPict = Image::Load("Data/Picture/Speed/Speeder.png");
	assert(_hPict >= 0);
	//黒ゲージ画像データのロード
	_hPict2 = Image::Load("Data/Picture/Speed/Not_Speed.png");
	assert(_hPict2 >= 0);
	//フレーム画像データのロード
	_hPict3 = Image::Load("Data/Picture/Speed/Speed_frame.png");
	assert(_hPict3 >= 0);

	_scale = D3DXVECTOR3(0.5f, 0.5f, 0.5f);

	_position = D3DXVECTOR3(960, 630, 0);

	

}

//更新
void Speed_meter::Update()
{

	//speedが50を上回るまで加速
	if (Input::IsKey(DIK_W) && x <= 50)
	{
		x = x + 2;
	}
	//離していると減速
	else if (x > 1)
	{
		x--;
	}

	//ブースト
	if (Input::IsKey(DIK_UP) && Boost >= 1)
	{
		x = 100;
		Boost--;
	}
	//ブースト増加(デバック用)
	if(Input::IsKey(DIK_DOWN) && Boost == 0)
	{
		Boost++;
	}

}

//描画
void Speed_meter::Draw()
{
	//
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, x*3.1f, 0, 0);
	D3DXMATRIX m2;
	D3DXMatrixTranslation(&m2, 200, 0, 0);
	
	

	//本ゲージ
	Image::SetMatrix(_hPict, _localMatrix);
	Image::Draw(_hPict);
	//黒ゲージ
	Image::SetMatrix(_hPict2, _localMatrix * m);
	Image::Draw(_hPict2);
	//フレーム
	Image::SetMatrix(_hPict3, _localMatrix);
	Image::Draw(_hPict3);


}

//開放
void Speed_meter::Release()
{
}