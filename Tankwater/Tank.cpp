#include "Tank.h"
#include "Engine/ResouceManager/Model.h"
#include "Ground.h"
#include "Engine/gameObject/Camera.h"
#include "PlayScene.h"
#include "Boost.h"
//障害あり
#include "Ground_item.h"
#include "Item.h"
#include "Block.h"


//コンストラクタ
Tank::Tank(IGameObject * parent)
	:IGameObject(parent, "Tank"), _hModel(-1)
	, _hGroundModel(-1), _hGroundModel2(-1),
	pEffect_(nullptr), pToonTex_(nullptr), speedcount(1),Boostcount(1)
	, viewChange(false)
{
}

//デストラクタ
Tank::~Tank()
{
	SAFE_RELEASE(pToonTex_);
	SAFE_RELEASE(pEffect_);
}

//初期化
void Tank::Initialize()
{
	LPD3DXBUFFER err = 0;
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::pDevice_,
		"ToonShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_,
		&err)))
	{
		MessageBox(NULL,
			(char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	//モデルデータのロード
	_hModel = Model::Load("data/Model/Player.fbx", pEffect_);
	assert(_hModel >= 0);


	//箱型の当たり判定を作る
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, 0.5f, 0), D3DXVECTOR3(1, 1, 1));
	AddCollider(collision);

	//アニメ風シェード
	D3DXCreateTextureFromFileEx(Direct3D::pDevice_,
		"data\\UV.png", 0, 0, 0, 0,
		D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
		D3DX_FILTER_NONE, D3DX_DEFAULT,
		NULL, NULL, NULL, &pToonTex_);


	//カメラ
	/*
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 1.0f, -0.3f));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
	*/
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -4));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
	//初期方向の修正
	_rotate.y = 180;
}



//更新
void Tank::Update()
{
	//移動
	Move();

	//地面に沿わせる
	FollowGround();

	//一個前の場所を覚えておく
	Pre_position = _position;

	//Itemがなくなったら壁を消す
	if (FindObject("Item") == nullptr)
	{
		((PlayScene*)GetParent())->GetBlock()->SetErasure(true);
	}
}

//移動
void Tank::Move()
{
	//戦車が向いている方向に回転させる行列
	D3DXMATRIX mRot;
	D3DXMatrixRotationY(&mRot, D3DXToRadian(_rotate.y));

	//移動ベクトル
	D3DXVECTOR3 move;
	D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);

	//加速用関数
	//speedが50を上回るまで加速
	if (Input::IsKey(DIK_W) && speedcount <= 50)
	{
		speedcount = speedcount + 2;
	}
	//離していると減速
	else if (speedcount > 1)
	{
		speedcount--;
	}

	//前進
	if (Input::IsKey(DIK_W))
	{
		_position = _position + move * (speedcount / 10);
	}
	//慣性が残って少しだけ進む感じに
	else
	{
		_position = _position + move * ((speedcount - 1) / 10);
	}

	if (Input::IsKeyUp(DIK_W))
	{
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, 0.2f), &mRot);
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		_position = _position - move;
	}

	//右旋回
	if (Input::IsKey(DIK_D))
	{
		_rotate.y += 1.0f;
	}

	//左旋回
	if (Input::IsKey(DIK_A))
	{
		_rotate.y -= 1.0f;
	}

	//ブーストの実装 最初複数形でも対応
	if(Input::IsKeyDown(DIK_UP) && Boostcount > 0)
	{
		//speedを100まで強制で上げる
		speedcount = 100;
		//Boostを一つ消費
		Boostcount--;
	}
	//なくしたブーストを追加(デバック用)
	if (Input::IsKeyUp(DIK_DOWN) && Boostcount == 0)
	{
		//Boostを一つ追加
		Boostcount++;
	}

	//カメラを一人称や三人称に変えたい
	if (Input::IsKeyDown(DIK_1))
	{
		//viewChange = true;
	}

	//右旋回
	if (Input::IsKey(DIK_RIGHT) && !Input::IsKey(DIK_LEFT))
	{
		_rotate.y += 1.5f;
		if (_rotate.z >= -20.0) {
			_rotate.z = _rotate.z - 2.0f;
		}
	}
	else if (_rotate.z <= 0.0f)
	{
		_rotate.z = _rotate.z + 2.0f;
	}

	//左旋回
	if (Input::IsKey(DIK_LEFT) && !Input::IsKey(DIK_RIGHT))
	{
		_rotate.y -= 1.5f;
		if (_rotate.z <= 20.0) {
			_rotate.z = _rotate.z + 2.0f;
		}
	
	}
	else if (_rotate.z >= 0.0f)
	{
		_rotate.z = _rotate.z - 2.0f;
	}

	//空も飛べるはず
	/*
	ジャンプの参考
	http://h25monka.gamelearning.jp/textbook/gs22-0202/chapter8.html
	*/
	/*if (Input::IsKey(DIK_SPACE)) 
	{
		_position.y += 0.1;
	}
	if (_position.y >= 0)
	{
		_position.y -= 0.05;

	}*/


	//Boostが0 = 使用不可能になった場合
	if (Boostcount == 0)
	{
		((PlayScene*)GetParent())->GetBoost()->SetBoost(true);
	}
	//今後ブーストが回復する可能性があるので回復した場合もとに戻す
	else
	{
		((PlayScene*)GetParent())->GetBoost()->SetBoost(false);
	}

	//CLEAR判定(仮)　5個そろえて外に出たら一旦CLEARで
	if(_position.z <= -90)
	{
		//移動
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);
	}

	//一人称
	if (viewChange == true)
	{
		Camera* pCamera = CreateGameObject<Camera>(this);
		pCamera->SetPosition(D3DXVECTOR3(_position.x, 1.0f + _position.y, -0.3f + _position.z));
		pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));
		pCamera->SetRotateY(180 + _rotate.y);
		viewChange = false;
	}
}


//地面に沿わせる
void Tank::FollowGround()
{
	//まだ地面のモデル番号を知らない
	//地面や壁の判定
	if (_hGroundModel == -1)
	{
		//モデル番号を調べる
		//_hGroundModel = ((Ground*)FindObject("Ground"))->GetModelHandle();
		_hGroundModel = ((Ground_item*)FindObject("Ground_item"))->GetModelHandle();
	}
	//障害物の判定
	if (_hGroundModel2 == -1)
	{
		//モデル番号を調べる
		_hGroundModel2 = ((Ground_item*)FindObject("Block"))->GetModelHandle();
	}

	
	//レイを撃つ準備
	RayCastData data;
	data.start = _position;					//戦車の原点から
	data.start.y = 10;						//高さ10（地面は一番高いところでも5になっている）
	data.dir = D3DXVECTOR3(0, -1, 0);		//真下方向

	//障害の壁用のレイ
	RayCastData data2;
	data2.start = _position;				//戦車の原点から
	data2.start.y = 10;						//高さ10（地面は一番高いところでも5になっている）
	data2.dir = D3DXVECTOR3(0, -1, 0);		//真下方向


	//地面に対してレイを撃つ		
	Model::RayCast(_hGroundModel, &data);
	Model::RayCast(_hGroundModel2, &data2);

	//レイが地面に当たったら
	if (data.hit || data2.hit)
	{
		//道より高いところにレイが当たった時
		if (data.dist < 6 || data2.dist < 6)
		{
			//元の場所に戻したい
			_position.x = Pre_position.x;
			_position.z = Pre_position.z;
		}
		//戦車の高さを地面にあわせる
		//（Y=10の高さからレイ撃って、data.distメートル先に地面があったということは
		//　そこの標高は『-data.distメートル + 10』ということになる）
		//_position.y = -data.dist + 10;
	}

}

//描画
void Tank::Draw()
{
	//ワールド行列のセット
	Model::SetMatrix(_hModel, _worldMatrix);
	//ビュー
	D3DXMATRIX view;
	Direct3D::pDevice_->GetTransform(D3DTS_VIEW, &view);
	//プロジェクション
	D3DXMATRIX proj;
	Direct3D::pDevice_->GetTransform(D3DTS_PROJECTION, &proj);

	//掛け算
	D3DXMATRIX matWVP = _worldMatrix * view * proj;

	//合成した行列をシェーダーに渡す
	pEffect_->SetMatrix("WVP", &matWVP);

	//オブジェクトの動きに合わせて法線を変形指せるためにワールド行列を渡す
	D3DXMATRIX mat = _worldMatrix;
	//範囲行列のx,y,zの値を固定	これでライトが一定になる
	//mat._41 = 0;
	//mat._42 = 0;
	//mat._43 = 0;
	//縦の判定も固定させたい

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(_rotate.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(_rotate.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(_rotate.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);
	D3DXMatrixInverse(&scale, nullptr, &scale);

	mat = scale * rotateZ * rotateX * rotateY;


	pEffect_->SetMatrix("RS", &mat);


	D3DLIGHTTYPE  Type;
	//ライトの向きをシェーダーに渡す
	D3DLIGHT9 lightState;
	Direct3D::pDevice_->GetLight(0, &lightState);
	pEffect_->SetVector("LIGHT_DIR",
		(D3DXVECTOR4*)&lightState.Direction);

	//カラーの変更
	//pEffect_->SetVector("DIFFUSE_COLOR",&D3DXVECTOR4(0, 1, 0,1));

	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 10, -5));

	//ワールド行列
	pEffect_->SetMatrix("W", &_worldMatrix);

	pEffect_->SetTexture("TEXTURE_TOON", pToonTex_);

	//UVスクロール
	static float scroll = 0.0f;
	scroll += 0.0001f;
	pEffect_->SetFloat("SCROLL", scroll);

	pEffect_->Begin(NULL, 0);



	//輪郭表示(エッジ)
	pEffect_->BeginPass(1);
	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	//Direct3D::pDevice_->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
	Model::Draw(_hModel);
	Direct3D::pDevice_->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//Direct3D::pDevice_->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);

	pEffect_->EndPass();

	//普通に表示
	pEffect_->BeginPass(0);
	Model::Draw(_hModel);
	pEffect_->EndPass();

	//シェーダーを使った描画おわり
	//これ以降はDirectXデフォルトの描写に戻る
	pEffect_->End();


}

//開放
void Tank::Release()
{
}