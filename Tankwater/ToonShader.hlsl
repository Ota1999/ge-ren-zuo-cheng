//頂点シェーダー

//グローバル変数(アプリ側から流される情報)
float4x4 WVP;
float4x4 RS;
float4x4 W;		//ワールド行列

float4 LIGHT_DIR;
float4 DIFFUSE_COLOR;
float4 AMBIENT_COLOR;
float4 SPECULER_COLOR;
float  SPECULER_POWER;
float4 CAMERA_POS;		//視点(カメラの位置)
bool   IS_TEXTURE;

texture TEXTURE;		//
texture TEXTURE_TOON;

//サンプラー
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler toonSampler = sampler_state
{
	Texture = <TEXTURE_TOON>;
	AddressU = Clamp;
	/*
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	*/
};

//構造体
//頂点シェーダーの出力でピクセルシェーダーの入力
struct VS_OUT
{
	float4 pos		:SV_POSITION;	//位置
	float4 normal	:NORMAL;		//法線
	float4 eye      :TEXCOORD1;		//視線
	float2 uv		:TEXCOORD0;	   //UV座標
};

//POSITONは頂点情報の中の位置座標を入れておく　セマンティクスと呼ぶ
//[頂点シェーダー]
//float4は4個セットになったベクトルのような型  float4 = ピクセルの色
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	//mayaで指定したところに動かした場合と同じ動き
	//こうしたらどうなるか
	/*
	pos.x += 1;
	*/

	//出力データ
	VS_OUT outData;

	outData.pos = mul(pos, WVP);//位置(今まで通り)

	normal = mul(normal, RS);	//オブジェクトが変化
	normal = normalize(normal);	//法線を正規化
	outData.normal = normal;

	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	outData.uv = uv;

	//まとめて出力
	return outData;
}
//ピクセルシェーダー　8回呼ばれる？
float4 PS(VS_OUT inData) : COLOR
{
	//縦の中心より右
	/*if (pos.x > 640){
		color.a = 0;
		}*/

		inData.normal = normalize(inData.normal);
		inData.eye = normalize(inData.eye);

		float4 lightDir = LIGHT_DIR;//左上手前
		lightDir = normalize(lightDir);//向きだけが必要ない

		//拡散反射光
		float u = dot(inData.normal, -lightDir);
		float4 diffuse = tex2D(toonSampler, float2(u, 0));
		//diffuse.a = 1;//暗い部分はαまで小さくなっている

		/*if (diffuse.r < 0.3) {
			diffuse = float4(0.5, 0.5, 0.5, 1);
		}
		else if (diffuse.r < 0.5) {
			diffuse = float4(0.7, 0.7, 0.7, 1);
		}else{
			diffuse = float4(1, 1, 1, 1);
		}*/

		//テクスチャを張っていた時
		if (IS_TEXTURE)
		{
			//テクスチャを張っていた時
			diffuse *= tex2D(texSampler, inData.uv);
		}
		diffuse *= DIFFUSE_COLOR;//アプリ側から受け取った色を設定

		//環境光
		//float4 ambient = float4(0.2, 0.2, 0.2, 0);//色の設定
		float4 ambient = AMBIENT_COLOR;
		//これでアンビエント光をなくせる
		ambient = float4(0, 0, 0, 0);

		//鏡面反射光
		//float speSize = 40.0f;
		//float spePower = 2.0f;


		float4 R = reflect(lightDir, inData.normal);
		//float4 speculer = pow(dot(R, inData.eye), speSize) * spePower;//powで乗算
		float4 speculer = pow(dot(R, inData.eye), SPECULER_POWER) * 2 * SPECULER_COLOR;
		//頂点シェーダーの出力でピクセルシェーダーの入力
		//return ambient + diffuse + speculer;
		//return diffuse;　//元これ

		//頂点シェーダーで計算した色のデータをそのまま返す
		//グローシェーディング
		return ambient + diffuse + speculer;
}

//輪郭をつける
float4 VS_Toon(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0) : SV_POSITION
{
	normal.w = 0;
	pos += normal / 20;
	pos = mul(pos, WVP);
	return pos;
}

float4 PS_Toon(float4 pos : SV_POSITION) : COLOR
{
	return float4(0,0,0,1);
}

//テクニック
//関数をピクセルシェーダー、頂点シェーダーとして登録
//エントリーポイントみたいなもの
technique
{
	//0番目
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
	//実行するたびにコンパイルするので、実行ファイルとシェーダーのソースを入れないといけない
	pass
	{
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader = compile ps_3_0 PS_Toon();
	}
}