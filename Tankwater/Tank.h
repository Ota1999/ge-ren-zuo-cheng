#pragma once
#include "Engine/GameObject/GameObject.h"

//戦車（プレイヤー）を管理するクラス
class Tank : public IGameObject
{
	int _hModel;		//モデル番号
	int _hModel2;		//影のモデル番号(先取)
	int _hGroundModel;	//地面のモデル番号
	int _hGroundModel2;	//地面のモデル番号二番目
	int speedcount;		//加速の管理
	int Boostcount;		//ブースト使用量
	bool viewChange;   //視点変更
	//移動
	void Move();

	//地面に沿わせる
	void FollowGround();

	LPD3DXEFFECT pEffect_;

	LPDIRECT3DTEXTURE9 pToonTex_;

	D3DXVECTOR3 Pre_position; //元の位置




public:
	Tank(IGameObject* parent);
	~Tank();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};