#pragma once

#include "Engine/global.h"

//ゲームオーバーシーンを管理するクラス
class GameoverScene : public IGameObject
{
	int _hPict;    //画像番号

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GameoverScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};