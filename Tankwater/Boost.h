#pragma once
#include "Engine/GameObject/GameObject.h"

//Boost表示を管理するクラス
class Boost : public IGameObject
{
	int _hPict;    //画像番号

	bool _Boost;		//falseとtrueで判断 使ったか使ってないか

public:
	Boost(IGameObject* parent);
	~Boost();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetBoost(bool boost_)
	{
		_Boost = boost_;
	}
};