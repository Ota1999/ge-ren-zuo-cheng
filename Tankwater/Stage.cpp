#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"

//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
	//モデル番号を初期化
	for (int i = 0; i < BL_MAX; i++)
	{
		_hModel[i] = -1;
	}
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//床モデルデータのロード
	_hModel[BL_FLOOR] = Model::Load("data/Floor.fbx");
	assert(_hModel[BL_FLOOR] >= 0);

	//壁モデルデータのロード
	_hModel[BL_WALL] = Model::Load("data/Wall.fbx");
	assert(_hModel[BL_WALL] >= 0);




	//CSVファイルからステージ情報をロード
	CsvReader csv;
	csv.Load("data/Map.csv");
	for (int x = 0; x < STAGE_SIZE; x++)
	{
		for (int z = 0; z < STAGE_SIZE; z++)
		{
			_table[x][z] = (BLOCK_TYPE)csv.GetValue(x, z);
		}
	}


	_scale = D3DXVECTOR3(2, 2, 2);
}

//更新
void Stage::Update()
{
	
}

//描画
void Stage::Draw()
{
	for (int x = 0; x < STAGE_SIZE; x++)
	{
		for (int z = 0; z < STAGE_SIZE; z++)
		{
			//壁か床か
			int type = _table[x][z];

			//平行移動行列を作成
			D3DXMATRIX mat;
			D3DXMatrixTranslation(&mat, x, 0, z);

			//描画
			Model::SetMatrix(type, mat);
			Model::Draw(type);
		}
	}
}

//開放
void Stage::Release()
{
}