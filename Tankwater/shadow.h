#pragma once
#include "Engine/GameObject/GameObject.h"

//影を管理するクラス
class shadow : public IGameObject
{
	//int _hModel;

	//LPD3DXEFFECT pEffect_;

public:
	shadow(IGameObject* parent);
	~shadow();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//モデル番号のゲッター
	//int GetModelHandle() { return _hModel; }
};