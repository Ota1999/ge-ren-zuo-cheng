#include "Boost.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Boost::Boost(IGameObject * parent)
	:IGameObject(parent, "Boost"), _hPict(-1)
	, _Boost(false)
{
}

//デストラクタ
Boost::~Boost()
{
}

//初期化
void Boost::Initialize()
{

	//ブースト使用可能か不可かの判定

	//画像データのロード
	_hPict = Image::Load("data/Picture/Boost1.png");
	assert(_hPict >= 0);
	
	_scale = D3DXVECTOR3(0.5f, 0.5f, 0.5f);
	_position = D3DXVECTOR3(30, 30, 0);
}

//更新
void Boost::Update()
{
	//ブーストを使った場合
	if (_Boost == true)
	{
		_hPict = Image::Load("data/Picture/Boost2.png");
		assert(_hPict >= 0);
	}//ブーストが復活した場合
	else
	{
		_hPict = Image::Load("data/Picture/Boost1.png");
		assert(_hPict >= 0);
	}
}

//描画
void Boost::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Boost::Release()
{
}