#include "Ground_item.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Ground_item::Ground_item(IGameObject * parent)
	:IGameObject(parent, "Ground_item"), _hModel(-1)
{
}

//デストラクタ
Ground_item::~Ground_item()
{
}

//初期化
void Ground_item::Initialize()
{
	//モデルデータのロード

	_hModel = Model::Load("data/Model_Item/Course_gim.fbx");
	//assert(_hModel >= 0);

}

//更新
void Ground_item::Update()
{

}

//描画
void Ground_item::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Ground_item::Release()
{
}