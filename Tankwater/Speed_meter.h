#pragma once

#include <string>
using namespace std;
#include "Engine/GameObject/GameObject.h"

//テキストのクラス
class Text;

//速度のメーターを管理するクラス
class Speed_meter : public IGameObject
{
	//時間の画像表記用記述
	//本ゲージ
	int _hPict;
	//黒ゲージ
	int _hPict2;
	//フレーム
	int _hPict3;


	//ゲージを動かすための仮
	int x;

	//ブースト用仮
	int Boost;

public:
	Speed_meter(IGameObject* parent);
	~Speed_meter();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};