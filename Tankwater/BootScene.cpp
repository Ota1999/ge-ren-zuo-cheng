#include "BootScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
BootScene::BootScene(IGameObject * parent)
	: IGameObject(parent, "BootScene"), _hPict(-1)
{
}

//初期化
void BootScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Picture/StartScreen.jpg");
	assert(_hPict >= 0);
}

//更新
void BootScene::Update()
{
	if (Input::IsKeyUp(DIK_SPACE))
	{
		//移動
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_MENU);
	}
}

//描画
void BootScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void BootScene::Release()
{
}