#pragma once
#include <string>
using namespace std;
#include "Engine/GameObject/GameObject.h"

//テキストのクラス
class Text;

//タイムを管理するクラス
class Time : public IGameObject
{
	//時間の画像表記用記述
	//秒
	int _hPict;
	//秒2桁目
	int _hPict2;
	//分
	int _hPict3;
	//コンマ
	int _hPict4;

	//それぞれのタイムの文字を送るように
	int _hTime_image[10];

	char *sec_[11];	//秒カウント用配列

	char *min_[11];	//分カウント用配列

	//1の位
	int cnt1_;
	//10の位
	int cnt10_;
	//分の位
	int minCnt_;
	//フレームをカウントする変数
	int frame_;

public:
	Time(IGameObject* parent);
	~Time();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};