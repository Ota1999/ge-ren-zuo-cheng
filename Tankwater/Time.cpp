#include "Time.h"
#include "Engine/DirectX/Text.h"
#include "Engine/ResouceManager/Image.h"
//コンストラクタ
Time::Time(IGameObject * parent)
	:IGameObject(parent, "Time")
	, _hPict(-1), _hPict2(-1), _hPict3(-1), _hPict4(-1),
	//cnt1_(10), cnt10_(10), minCnt_(10)//時間カウント用
	cnt1_(0), cnt10_(0), minCnt_(9)     //タイムアウト系
	,frame_(0)
{
}

//デストラクタ
Time::~Time()
{
	
}

//初期化
void Time::Initialize()
{
	std::string timeName[] =
	{
		"Data/Picture/Time_0.png",
		"Data/Picture/Time_9.png",
		"Data/Picture/Time_8.png",
		"Data/Picture/Time_7.png",
		"Data/Picture/Time_6.png",
		"Data/Picture/Time_5.png",
		"Data/Picture/Time_4.png",
		"Data/Picture/Time_3.png",
		"Data/Picture/Time_2.png",
		"Data/Picture/Time_1.png",
		"Data/Picture/Time_0.png",
	};

	//秒
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	//分
	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";


	for (int i = 0; i < 10; i++)
	{
		_hTime_image[i] = Image::Load(timeName[i]);
	}

	//////////////////////////////////////////////////
	//画像表示関連
	_position = D3DXVECTOR3(30, 30, 0);

	//画像データのロード
	_hPict = Image::Load("Data/Picture/Time_0.png");
	assert(_hPict >= 0);
	//画像データのロード
	_hPict2 = Image::Load("Data/Picture/Time_0.png");
	assert(_hPict2 >= 0);
	//画像データのロード
	_hPict3 = Image::Load("Data/Picture/Time_0.png");
	assert(_hPict3 >= 0);
	//画像データのロード
	_hPict4 = Image::Load("data/Picture/Time_kome.png");
	assert(_hPict4 >= 0);

	_scale = D3DXVECTOR3(0.2f, 0.2f, 0.2f);

	_position = D3DXVECTOR3(550, 30, 0);

	//minCnt_ = 7;
	//cnt10_ = cnt1_ = 0;



}

//更新
void Time::Update()
{
	//繰り上がりよう
	/*
	//フレームのカウントが60でちょうど1秒
	frame_++;
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ
		if (minCnt_ >= 0)
		{
			//cnt10は10の位のカウント
			if (cnt10_ >= 0)
			{
				//1の位が9だったら
				if (cnt1_ == 1)
				{
					//10の位が1進む
					cnt10_--;
				}
				//10の位が5でかつ1の位が9の時
				if (cnt10_ == 4 && cnt1_ == 1)
				{
					//10の位0
					cnt10_ = 10;
					//1の位0
					cnt1_ = 10;
					//分を1増加
					minCnt_--;
				}
				//1の位のカウント
				else if (cnt1_ > 0)
				{
					//1の位が1進む
					cnt1_--;
				}
				//1の位が繰り上がったら
				else if (cnt1_ == 0)
				{
					//1の位を1に
					cnt1_ = 9;
				}
			}
		}
	}
	//繰り上がり*/

	//タイムアップ
	frame_++;
	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{
			//最初1:00から0:59にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 5;
				cnt1_ = 0;
				minCnt_++;
			}
			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 5;
					cnt1_ = 1;
					minCnt_++;
				}
				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}
				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}
			}
		}
	}

	//0分になったらゲームオーバー
	if (cnt1_ == 10 && cnt10_ == 10 && minCnt_ == 10 || Input::IsKey(DIK_G))
	{
		SceneManager* pSm = (SceneManager*)FindObject("SceneManager");
		pSm->ChangeScene(SCENE_ID_GAMEOVER);
	}

}

//描画
void Time::Draw()
{
	//
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 120, 0, 0);
	D3DXMATRIX m2;
	D3DXMatrixTranslation(&m2, 200, 0, 0);
	D3DXMATRIX m3;
	D3DXMatrixTranslation(&m3, 60, 0, 0);
	//Draw(715, 20, min_[minCnt_] + sec_[cnt10_] + sec_[cnt1_]); //
	if (minCnt_ == 10)
	{
		Image::SetMatrix(_hPict3, _localMatrix);
		Image::Draw(_hPict3);
	}
	else {
		Image::SetMatrix(_hTime_image[minCnt_], _localMatrix);
		Image::Draw(_hTime_image[minCnt_]);
	}

	if (cnt10_ == 10)
	{
		Image::SetMatrix(_hPict2, _localMatrix * m);
		Image::Draw(_hPict2);
	}
	else {
		Image::SetMatrix(_hTime_image[cnt10_], _localMatrix * m);
		Image::Draw(_hTime_image[cnt10_]);
	}
	if (cnt1_ == 10)
	{
		Image::SetMatrix(_hPict, _localMatrix * m2);
		Image::Draw(_hPict);
	}
	else {
		Image::SetMatrix(_hTime_image[cnt1_], _localMatrix * m2);
		Image::Draw(_hTime_image[cnt1_]);
	}
	Image::SetMatrix(_hPict4, _localMatrix * m3);
	Image::Draw(_hPict4);

}

//開放
void Time::Release()
{
}