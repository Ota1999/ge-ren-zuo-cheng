#include "Infomation.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
Infomation::Infomation(IGameObject * parent)
	:IGameObject(parent, "Infomation"), _hPict(-1)
{
}

//デストラクタ
Infomation::~Infomation()
{
}

//初期化
void Infomation::Initialize()
{
	/*
	//画像データのロード
	_hPict = Image::Load("data/Picture/info.png");
	assert(_hPict >= 0);
	*/

	_position = D3DXVECTOR3(30, 30, 0);
}

//更新
void Infomation::Update()
{
}

//描画
void Infomation::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void Infomation::Release()
{
}