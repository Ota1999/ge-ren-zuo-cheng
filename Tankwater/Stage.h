#pragma once

//インクルード
#include "Engine/GameObject/GameObject.h"

//定数
#define STAGE_SIZE	15		//ステージのサイズ（1辺の長さ）


//ステージを管理するクラス
class Stage : public IGameObject
{
	///////////////////////　定数　/////////////////////////////
	enum BLOCK_TYPE
	{
		BL_FLOOR,	//床
		BL_WALL,	//壁
		BL_MAX
	};

	///////////////////////　変数　/////////////////////////////
	int			_hModel[BL_MAX];				//モデル番号
	BLOCK_TYPE	_table[STAGE_SIZE][STAGE_SIZE];	//ステージ情報



	///////////////////////　public関数　/////////////////////////////
public:
	//コンストラクタ
	Stage(IGameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//そこが壁かどうか
	//引数：x,z	調べる位置
	//戻値：壁ならtrue、床ならfalse
	bool IsWall(int x, int z)
	{
		return _table[x][z] == BL_WALL;
	}
};