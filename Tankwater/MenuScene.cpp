#include "MenuScene.h"
#include "Engine/ResouceManager/Image.h"
#include "Ground_item.h"

//コンストラクタ
MenuScene::MenuScene(IGameObject * parent)
	: IGameObject(parent, "MenuScene"), _hPict(-1)
{
}

//初期化
void MenuScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Picture/MenuScreen.jpg");
	assert(_hPict >= 0);


	CreateGameObject<Ground_item>(this);
}

//更新
void MenuScene::Update()
{


	if (Input::IsKeyUp(DIK_SPACE))
	{
		//移動
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void MenuScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void MenuScene::Release()
{
}