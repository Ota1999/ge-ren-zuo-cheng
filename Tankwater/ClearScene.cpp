#include "ClearScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
ClearScene::ClearScene(IGameObject * parent)
	: IGameObject(parent, "ClearScene"), _hPict(-1)
{
}

//初期化
void ClearScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Picture/ClearScreen.jpg");
	assert(_hPict >= 0);
}

//更新
void ClearScene::Update()
{
	//タイトルに戻る
	if (Input::IsKeyUp(DIK_SPACE))
	{
		//移動
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_BOOT);
	}
}

//描画
void ClearScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void ClearScene::Release()
{
}