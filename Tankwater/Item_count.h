#pragma once
#include <string>
using namespace std;
#include "Engine/GameObject/GameObject.h"

//テキストのクラス
class Text;

//アイテムの数を管理するクラス
class Item_count : public IGameObject
{
	//残りの文字
	int _hPict;
	//残りの数
	int _hPict2;

	//画像事前に呼び出し
	int _hItem_countName[6];

	//画像をどんどん入れ替える用
	int COU;

	//画像を変える
	bool _Count;


public:
	Item_count(IGameObject* parent);
	~Item_count();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetItem_Count(bool count)
	{
		_Count = count;
	}
};