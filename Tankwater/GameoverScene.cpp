#include "GameoverScene.h"
#include "Engine/ResouceManager/Image.h"

//コンストラクタ
GameoverScene::GameoverScene(IGameObject * parent)
	: IGameObject(parent, "GameoverScene"), _hPict(-1)
{
}

//初期化
void GameoverScene::Initialize()
{
	//画像データのロード
	_hPict = Image::Load("data/Picture/GameoverScreen.jpg");
	assert(_hPict >= 0);
}

//更新
void GameoverScene::Update()
{
	//タイトルに戻る
	if (Input::IsKeyUp(DIK_SPACE))
	{
		//移動
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_BOOT);
	}
}

//描画
void GameoverScene::Draw()
{
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
}

//開放
void GameoverScene::Release()
{
}