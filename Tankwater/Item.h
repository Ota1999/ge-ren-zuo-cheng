#pragma once
#include "Engine/GameObject/GameObject.h"

//アイテムを管理するクラス
class Item : public IGameObject
{
	int _hModel;    //モデル番号

	LPD3DXEFFECT pEffect_;


private:
	int _hSound;    //サウンド番号

public:
	Item(IGameObject* parent);
	~Item();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;
};