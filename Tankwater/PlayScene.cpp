#include "PlayScene.h"
#include "Ground.h"
#include "Tank.h"
#include "Infomation.h"
#include "Boost.h"
#include "Stage.h"
#include "Time.h"
#include "Speed_meter.h"
#include "Item_count.h"
#include <string>

//アイテムギミックのあるやつ
#include "Block.h"
#include "Ground_item.h"
#include "Item.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"),ITEM_NUM(5)
{
}

//初期化
void PlayScene::Initialize()
{
	//一般ルート
	/*
	CreateGameObject<Ground>(this);
	CreateGameObject<Stage>(this);

	//*/

	//ギミックルート 消したいとき→のやつを改行して対応/*


	CreateGameObject<Ground_item>(this);
	for (int i = 0; i < ITEM_NUM; i++)
	{
		CreateGameObject<Item>(this);
	}
	pBlock = CreateGameObject<Block>(this);

	//*/ //改行けしてもらって

	//本体
	pTank = CreateGameObject<Tank>(this);
	//ブースト管理
	pBoost = CreateGameObject<Boost>(this);
	//タイム
	CreateGameObject<Time>(this);
	
	//速度の目安用
	CreateGameObject<Speed_meter>(this);

	//残りアイテムの表示
	pItem_Count = CreateGameObject<Item_count>(this);
	
	//多分使ってない
	CreateGameObject<Infomation>(this);


	//メモリリーク探すよう
	//new char[1];
	//new char[2];


	//敵がいないのでとりあえずESCでクリア画面に移行する

}

//更新
void PlayScene::Update()
{
	if (Input::IsKey(DIK_ESCAPE))
	{
		SceneManager* pSm = (SceneManager*)FindObject("SceneManager");
		pSm->ChangeScene(SCENE_ID_CLEAR);
	}
}

//描画
void PlayScene::Draw()
{

}

//開放
void PlayScene::Release()
{
}
