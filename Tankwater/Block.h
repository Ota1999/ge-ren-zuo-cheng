#pragma once
#include "Engine/GameObject/GameObject.h"

//障害物を管理するクラス
class Block : public IGameObject
{
	int _hModel;

	LPD3DXEFFECT pEffect_;

	bool erasure_;		//falseとtrueで判断


private:
	int _hSound;    //サウンド番号

public:
	Block(IGameObject* parent);
	~Block();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//モデル番号のゲッター
	int GetModelHandle() { return _hModel; }

	void SetErasure(bool erasure)
	{
		erasure_ = erasure;
	}
};