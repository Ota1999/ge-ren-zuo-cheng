#include "Block.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
Block::Block(IGameObject * parent)
	:IGameObject(parent, "Block"), _hModel(-1), _hSound(-1),
	pEffect_(nullptr)
	, erasure_(false)
{
}

//デストラクタ
Block::~Block()
{
	SAFE_RELEASE(pEffect_);
}

//初期化
void Block::Initialize()
{
	//モデルデータのロード

	_hModel = Model::Load("data/Model_Item/Block.fbx", pEffect_);
	assert(_hModel >= 0);

	_position.z = -87.5;

	//サウンドデータのロード
	_hSound = Audio::Load("data/Audio/autodoor-open1.wav");
	assert(_hSound >= 0);

}

//更新
void Block::Update()
{
	//アイテムをすべて獲得した場合下にずれて通れるようにする
	if (erasure_ == true && _position.y >= -5)
	{
		//erasure_ = false;
		_position.y--;
		//音を鳴らす
		Audio::Play(_hSound);

		//KillMe();
	}

}

//描画
void Block::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Block::Release()
{

}