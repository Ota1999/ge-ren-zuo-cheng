#include "Item_count.h"
#include "Engine/DirectX/Text.h"
#include "Engine/ResouceManager/Image.h"

#include "Item.h"

//コンストラクタ
Item_count::Item_count(IGameObject * parent)
	:IGameObject(parent, "Item_count")
	, _hPict(-1), _hPict2(-1)
	,_Count(false),COU(0)
{
}

//デストラクタ
Item_count::~Item_count()
{

}

//初期化
void Item_count::Initialize()
{
	std::string Item_countName[] =
	{
		"Data/Picture/Count/Item0.png",
		"Data/Picture/Count/Item1.png",
		"Data/Picture/Count/Item2.png",
		"Data/Picture/Count/Item3.png",
		"Data/Picture/Count/Item4.png",
		"Data/Picture/Count/Item5.png",
	};

	//先に入れておく
	for (int i = 0; i < 6; i++)
	{
		_hItem_countName[i] = Image::Load(Item_countName[i]);
	}


	//////////////////////////////////////////////////

	//文字の表示のロード
	_hPict = Image::Load("Data/Picture/Count/Open.png");
	assert(_hPict >= 0);
	//残り数の表示のロード
	_hPict2 = Image::Load("Data/Picture/Count/Item0.png");
	assert(_hPict2 >= 0);

	_scale = D3DXVECTOR3(0.5f, 0.5f, 0.5f);


	//画像表示関連
	_position = D3DXVECTOR3(950, 30, 0);

}

//更新
void Item_count::Update()
{
	if (_Count == true)
	{
		COU++;
		_Count = false;
	}

	if (FindObject("Item") == nullptr)
	{
		_hPict = Image::Load("Data/Picture/Count/Open2.png");
		assert(_hPict >= 0);
	}
}

//描画
void Item_count::Draw()
{
	//
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, 120, 0, 0);
	D3DXMATRIX m2;
	D3DXMatrixTranslation(&m2, 200, 0, 0);

	//文字の表示
	Image::SetMatrix(_hPict, _worldMatrix);
	Image::Draw(_hPict);
	//残り数の表示
	if (COU == 0) {
		Image::SetMatrix(_hPict2, _localMatrix);
		Image::Draw(_hPict2);
	}
	else {
		Image::SetMatrix(_hItem_countName[COU], _localMatrix);
		Image::Draw(_hItem_countName[COU]);
	}
}

//開放
void Item_count::Release()
{
}