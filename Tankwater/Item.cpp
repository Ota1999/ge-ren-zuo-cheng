#include "Item.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Ground.h"
#include "Ground_item.h"
#include "Block.h"
#include "PlayScene.h"
#include "Item_count.h"
#include "shadow.h"

//コンストラクタ
Item::Item(IGameObject * parent)
	:IGameObject(parent, "Item"), _hModel(-1)
	,_hSound(-1)
	,pEffect_(nullptr)
{
}

//デストラクタ
Item::~Item()
{
	SAFE_RELEASE(pEffect_);
}

//初期化
void Item::Initialize()
{

	//CreateGameObject<shadow>(this);

	//モデルデータのロード
	_hModel = Model::Load("data/Model_Item/Item.fbx", pEffect_);
	assert(_hModel >= 0);

	//サイズ
	_scale *= 2;

	//位置
	_position.x = rand() % 70 - 40;
	_position.z = rand() % 65 - 80;

	//地面の高さ
	RayCastData data;
	data.start = _position;
	data.dir = D3DXVECTOR3(0, -1, 0);
	int hStageModel = ((Ground_item*)FindObject("Ground_item"))->GetModelHandle();
	Model::RayCast(hStageModel, &data);
	_position.y = -data.dist;

	//ちょっと高くする
	_position.y += 1.5;

	//向き
	_rotate.y = rand() % 360;


	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 1.2f);
	AddCollider(collision);

	//サウンドデータのロード
	_hSound = Audio::Load("data/Audio/decision5.wav");
	assert(_hSound >= 0);
}

//更新
void Item::Update()
{
}

//描画
void Item::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void Item::Release()
{
}


//何かに当たった
void Item::OnCollision(IGameObject * pTarget)
{
	//ラスト以外でプレイヤー（戦車）に当たった
	if (pTarget->GetObjectName() == "Tank")
	{
		//音を鳴らす
		Audio::Play(_hSound);
		//自分を消す
		KillMe();

		//自分が消えるときにItem_CountをTrueにしてカウントを進めさせる
		((PlayScene*)GetParent())->GetItem_Count()->SetItem_Count(true);
	}
}